const express = require('express');
const path = require('path');
const http = require('http');
const bodyParser = require('body-parser');
const router = require('./routes/routes');
var mongoose = require('mongoose');

var cors = require('cors');
const app = express();
const port = 3000;

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/companydatabase',{ useMongoClient: true });

mongoose.connection.on('connected', function () {  
  console.log('Mongoose connected');
});
mongoose.connection.on('error',function (err) {  
  console.log('Database connection error');
});  
app.use(cors());
app.use(bodyParser.json()); 
app.use('/api',router);

app.use(express.static(__dirname + '/public'));
app.get('*', function(req, res) {
  res.sendFile('./public/index.html', {"root": __dirname});
});

app.listen(port);               
console.log("working");
exports = module.exports = app;   