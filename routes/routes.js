const express = require('express');
const Company = require('../modelDB/database');
const mc_query = require('./mc_db_query');
const routeScraper = require('./routeScraper');
const scrapedResponse = routeScraper.scraped_response;
const partialName = require('./partial-name');
const googleFinance = require('./googlefinance');
const balanceSheetScraper = require('./mc_balancesheet_scraper');
const profitSheetScraper = require('./mc_profitsheet_scraper');
const newCalculator = require('./new_calculator');
const evaluator = require('./evaluation');
const MoneycontrolMain = require('./money-control/main');
const path = require('path');
var fs = require('fs');
const router = express.Router();
var baseurl = 'http://www.moneycontrol.com/financials/';   //url for balance sheet and profit sheet of money control
//routes_functionality

router.get('/suggestions/:string', function (req, res, next) {
    var name = req.params.string;
    var db_response;
    mc_query.getSuggestions(name).then((suggestion) => {
        res.json(suggestion);
        console.log(suggestion);
    });
});


router.get('/checkcompliance/:string', function (req, res) {
    var name = req.params.string;
    console.log(name);
    (async()=>{
        var frontend_data = await MoneycontrolMain.main_exec(name);//main_exec is called from money-control/main.js
    console.log(frontend_data);
        res.json(frontend_data);//json response to fronend containing moneycontrol company data plus google finance data
    })();
/*
    var db_response;
    var respObject = {};
    (async () => {
        try {
            db_response = await mc_query.getCompany(name); // waiting for database response
            respObject['company_name'] = db_response.company_name;
            respObject['bse'] = db_response.bse;
            respObject['nse'] = db_response.nse;
            respObject['iscompliant'] = db_response.iscompliant;
            respObject['comPar'] = db_response.comPar;
            respObject['nComPar'] = db_response.nComPar;
            var googleFinanceUrl = 'https://finance.google.com/finance?q=' + 'BOM:' + respObject.bse;
            respObject['googlefinance'] = await googleFinance.getGoogleFinance(googleFinanceUrl);
            res.json(respObject);
            console.log(respObject);
        }
        catch (error) {
            mc_query.getCompanyDetails(name).then((companyDetails) => {
                var balance_sheet_url = baseurl + companyDetails.url_name + '/balance-sheetVI/' + companyDetails.comp_code + '#' + companyDetails.comp_code;
                var profit_sheet_url = baseurl + companyDetails.url_name + '/profit-lossVI/' + companyDetails.comp_code + '#' + companyDetails.comp_code;
                var googleFinanceUrl = 'https://finance.google.com/finance?q=' + 'BOM:' + companyDetails.bse;
                var scrape_urls = [balance_sheet_url, profit_sheet_url];
                //new below
                var balanceSheetPromise = balanceSheetScraper.balanceSheetScrape(balance_sheet_url);
                var profitSheetPromise = profitSheetScraper.profitSheetScrape(profit_sheet_url);
                Promise.all([balanceSheetPromise, profitSheetPromise]).then(values => {
                    var balanceSheetObject = values[0];
                    if (balanceSheetObject.status) {
                        res.json(balanceSheetObject);
                    }
                    else {
                        var profitSheetObject = values[1];
                        for (var items in profitSheetObject) {
                            balanceSheetObject[items] = profitSheetObject[items];
                        }
                        var calculatedData = newCalculator.calculate(balanceSheetObject);
                        var evaluatedData = evaluator.evaluate(calculatedData);
                        if (companyDetails.bse === 'na' || companyDetails.bse === 'undefined' || companyDetails.bse === '0') {

                            res.json(evaluatedData);
                        }
                        else {
                            googleFinance.getGoogleFinance(googleFinanceUrl).then((googleFinanceData) => {
                                //here we are appnding newly received google finance data
                                // to the existing (evaluated data) money control data.
                                evaluatedData["googlefinance"] = googleFinanceData;
                                res.json(evaluatedData);
                            });
                        }
                    }
                });
                // routeScraper.initScraper(name,scrape_urls, res);
            }).catch(() => {
                var scrape_urls = ['null'];
                //routeScraper.initScraper(name, scrape_urls, res);
                (async () => {
                    try {
                        let bal_sht_prof_sht_urls = await partialName.initScraper(name, res);





                        var balanceSheetPromise = balanceSheetScraper.balanceSheetScrape(bal_sht_prof_sht_urls[0]);
                        var profitSheetPromise = profitSheetScraper.profitSheetScrape(bal_sht_prof_sht_urls[1]);
                        Promise.all([balanceSheetPromise, profitSheetPromise]).then(values => {
                            var balanceSheetObject = values[0];
                            if (balanceSheetObject.status) {
                                res.json(balanceSheetObject);
                            }
                            else {
                                var profitSheetObject = values[1];
                                for (var items in profitSheetObject) {
                                    balanceSheetObject[items] = profitSheetObject[items];
                                }
                                var calculatedData = newCalculator.calculate(balanceSheetObject);
                                var evaluatedData = evaluator.evaluate(calculatedData);
                                //  if (companyDetails.bse === 'na' || companyDetails.bse === 'undefined' || companyDetails.bse === '0') {

                                res.json(evaluatedData);
                                /* }
                                 else {
                                     googleFinance.getGoogleFinance(googleFinanceUrl).then((googleFinanceData) => {
                                         //here we are appnding newly received google finance data
                                         // to the existing (evaluated data) money control data.
                                         evaluatedData["googlefinance"] = googleFinanceData;
                                         res.json(evaluatedData);
                                     });
                                 }================*/
                                 /*
                            }
                        });






                    }
                    catch (no_name_matched) {
                        res.json(no_name_matched);
                    }
                })();
            });
        }
    })();*/
});

module.exports = router;