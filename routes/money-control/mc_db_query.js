const CompanyModelMc = require('../../modelDB/database');
const suggestionModelMc = require('../../modelDB/mcschemas/suggestionschema');




module.exports = mc_query = {

    getCompany: async function (name) {
        return new Promise(
            (resolve, reject) => {
                console.log('company entered:' + name);
                CompanyModelMc.findOne({ company_name: name }, function (err, company) {
                    if (err) {
                        reject();
                        console.log("error occured in database query");
                    }
                    else if (company === null) {
                        console.log("company is null");
                        reject();
                    }
                    else {
                        resolve(company);
                    }
                });
            }
        );
    },

    getSuggestions: function (name) {

        var strict_name = "^" + name;
        return new Promise((resolve, reject) => {
            suggestionModelMc.find({ company_name: { $regex: strict_name, $options: 'i' } },
                { company_name: 1, _id: 0 },
                { limit: 5 },
                function (err, suggestions) {
                    if (!err) {
                        resolve(suggestions);
                    }
                    else {
                        console.log('error in suggestions');
                    }
                });
        });
    },
    getCompanyDetails: function (name) {
        var exact_name = "^" + name + "$";
        return new Promise((resolve, reject) => {
            suggestionModelMc.findOne({ company_name: { $regex: exact_name, $options: 'i' } },
                { _id: 0 },
                function (err, companyDetails) {
                    if (!err) {
                        if (companyDetails === null) {
                            reject();
                        }
                        else {
                            resolve(companyDetails);
                        }

                    }
                    else {
                        console.log('company details could not be retreived');
                        reject();
                    }
                }
            );
        });
    },

    addCompany: function (companyobject) {
        console.log('it is in addcompany');
        CompanyModelMc.collection.insert(companyobject, function (error, company) {
            if (error) {
                console.log('problem adding data to database');
            }
            else {
                console.log('potatoes were successfully stored.');
            }
        });
    }
}   //END OF MODULE