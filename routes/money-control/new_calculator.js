


module.exports = {
    calculate: function (raw_matrices) {
        var bigArray = [raw_matrices];
        for (var item in bigArray) {
            //if (bigArray["short_term_borrowings"] === null) { console.log('short term borrowing missing'); }
            var databaseObject = {};
            var debt_value = Number(bigArray[item].long_term_borrowings) + Number(bigArray[item].short_term_borrowings) + Number(bigArray[item].other_current_liabilities);
            var total_assets = Number(bigArray[item].total_assets);
            if (total_assets == 0) {
                var debt = 0;
                var receivables = 0;
            }
            else {
                var debt = Math.round((debt_value / total_assets * 100) * 100) / 100;
                var receivables = Number(bigArray[item].cash_and_cash_equivalents) + Number(bigArray[item].trade_receivables);
            }
            var accounts_receivables = Math.round((receivables / total_assets * 100) * 100) / 100;
            if ((Number(bigArray[item].total_revenue) * 100) == 0) {
                var interest_based_income = 0;
            }
            else {
                var interest_based_income = Math.round((Number(bigArray[item].other_income) / Number(bigArray[item].total_revenue) * 100) * 100) / 100;
            }
            var company_name = bigArray[item].company_name;
            var iscompliant = 'null';
            var comPar = [];
            var nComPar = [];
           

            databaseObject['company_name'] = company_name;
            databaseObject['bse'] = bigArray[item].bse;
            databaseObject['nse'] = bigArray[item].nse;
            databaseObject['industry'] = bigArray[item].industry;
            databaseObject['iscompliant'] = iscompliant;
            databaseObject['accounts_receivables'] = accounts_receivables;
            databaseObject['debt'] = debt;
            databaseObject['interest_based_income'] = interest_based_income;
            databaseObject['comPar'] = comPar;
            databaseObject['nComPar'] = nComPar;
           return(databaseObject);  //defined in scrapper.js
            //console.log(databaseObject);
        }
    }


}