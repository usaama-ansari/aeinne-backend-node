



const Company = require('../../modelDB/database');
const mc_query = require('./mc_db_query');
const partialNameScript = require('./partial-name');
const googleFinance = require('./googlefinance');
const balanceSheetScraper = require('./mc_balancesheet_scraper');
const profitSheetScraper = require('./mc_profitsheet_scraper');
const newCalculator = require('./new_calculator');
const evaluator = require('./evaluation');
var baseurl = 'http://www.moneycontrol.com/financials/';
var google_finance_url = 'https://finance.google.com/finance?q=BOM:';


module.exports = {

    main_exec: function (name) {
        google_finance_url = 'https://finance.google.com/finance?q=BOM:';
        return new Promise((resolve, reject) => {
            var db_response;
            var respObject = {};
            (async () => {
                try {
                    // code for looking into database first for already saved companies
                    db_response = await mc_query.getCompany(name); // waiting for database response
                    respObject['company_name'] = db_response.company_name;
                    respObject['bse'] = db_response.bse;
                    respObject['nse'] = db_response.nse;
                    respObject['iscompliant'] = db_response.iscompliant;
                    respObject['comPar'] = db_response.comPar;
                    respObject['nComPar'] = db_response.nComPar;
                    google_finance_url = google_finance_url + respObject.bse;
                    //after receiving money control evaluated data from database we get google finance data
                    // through scraper below and send consolidated response in the form of respObject
                    respObject['googlefinance'] = await googleFinance.getGoogleFinance(google_finance_url);
                    resolve(respObject);
                    console.log(respObject);
                }
                // if query from database returns nothing then we use scraper for moneycontrol for new companies 
                // in case where name is passed from suggestion into the input box
                catch (error) {
                    mc_query.getCompanyDetails(name).then((companyDetails) => {
                        var balance_sheet_url = baseurl + companyDetails.url_name + '/balance-sheetVI/' + companyDetails.comp_code + '#' + companyDetails.comp_code;
                        var profit_sheet_url = baseurl + companyDetails.url_name + '/profit-lossVI/' + companyDetails.comp_code + '#' + companyDetails.comp_code;
                        scrape_all_three(balance_sheet_url, profit_sheet_url, resolve);
                    }).catch(() => {
                        (async () => {
                            try {
                                let bal_sht_prof_sht_urls = await partialNameScript.initScraper(name);
                                var balance_sheet_url = bal_sht_prof_sht_urls[0]
                                var profit_sheet_url = bal_sht_prof_sht_urls[1]
                                scrape_all_three(balance_sheet_url, profit_sheet_url, resolve);
                            }
                            catch (no_name_matched) {
                                resolve(no_name_matched);
                            }
                        })();
                    });
                }
            })();
        });
    }
};


// ==================================     VERY IMPORTANT FUNCTION BELOW        ====================================//

// ==================================    **** OPERATIONS PERFORMED ****        =================================//
// ==================================     BALANCE SHEET DATA SCRAPING        ====================================//
// ==================================     PROFIT SHEET DATA SCRAPING        =====================================//
// ==================================     ADDING NEW COMPANIES TO DATABASE     =====================================//
// here is the handy function to initiate three scraping script from three js files in money-control folder namely
//mc_balancesheet_scraper.js
//mc_profitsheet_scraper.js
//googlefinance.js
//this function is used in both cases of scraping  : when name is directly from suggestions
//                                                 : when random or partial name is typed and submitted. 

var scrape_all_three = async function (balance_sheet_url, profit_sheet_url, resolve) {
    var balanceSheetPromise = balanceSheetScraper.balanceSheetScrape(balance_sheet_url);
    var profitSheetPromise = profitSheetScraper.profitSheetScrape(profit_sheet_url);
    Promise.all([balanceSheetPromise, profitSheetPromise]).then(values => {
        var balanceSheetObject = values[0];
        if (balanceSheetObject.status) {
            resolve(balanceSheetObject);
        }
        else {
            var profitSheetObject = values[1];
            for (var items in profitSheetObject) {
                balanceSheetObject[items] = profitSheetObject[items];
            }
            var calculatedData = newCalculator.calculate(balanceSheetObject);
            var evaluatedData = evaluator.evaluate(calculatedData);
            if (evaluatedData.bse === 'na' || evaluatedData.bse === 'undefined' || evaluatedData.bse === '0') {
                resolve(evaluatedData);
            }
            else {
                google_finance_url = google_finance_url + evaluatedData.bse;
                googleFinance.getGoogleFinance(google_finance_url).then((googleFinanceData) => {
                    //here we are appnding newly received google finance data
                    // to the existing (evaluated data) money control data.
                    evaluatedData["googlefinance"] = googleFinanceData;
                    resolve(evaluatedData);
                });
            }
        }
    });
}

