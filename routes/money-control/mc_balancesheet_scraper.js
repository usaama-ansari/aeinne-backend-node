var request = require('request');
var cheerio = require('cheerio');





module.exports = {

    balanceSheetScrape: function (balance_sheet_url) {
        return new Promise((resolve, reject) => {
            request(balance_sheet_url, function (error, response, html) {
                if (!error) {
                    balanceSheetScrapedData = {};
                    var $ = cheerio.load(html);
                    var company_name = $('#nChrtPrc h1').text();
                    //bse nse retreival code upto line 100
                    var bsense = $('#nChrtPrc div.FL.gry10').text();
                    console.log(bsense);
                    var bsenseArr = bsense.split("|");
                    if (bsenseArr.length < 3) {// if the campany does not have sector like in case of BSE200
                        var page_missing = { "status": "Sorry! no data available for Balance sheet of "+company_name, "msg": "source missing" };
                        resolve(page_missing);
                    }
                    else {    // normal execution 
                        if (bsenseArr[0].match('BSE:')) {
                            balanceSheetScrapedData['bse'] = bsenseArr[0].replace(/BSE:\s/, '');
                            if (bsenseArr[1].match('NSE:')) {
                                balanceSheetScrapedData['nse'] = bsenseArr[1].replace(/NSE:\s/, '');
                            }
                            else {
                                balanceSheetScrapedData['nse'] = 'na';
                            }
                        }
                        else if (bsenseArr[0].match('NSE:')) {
                            balanceSheetScrapedData['bse'] = 'na';
                            balanceSheetScrapedData['nse'] = bsenseArr[0].replace(/NSE:\s/, '');
                        }
                        balanceSheetScrapedData['company_name'] = company_name;
                        console.log('stoch exchange data =' + balanceSheetScrapedData.bse + "|" + balanceSheetScrapedData.nse);
                        //industry retreival code upto line 108
                        if (bsenseArr[2].match('SECTOR:')) {
                            balanceSheetScrapedData['industry'] = bsenseArr[2].replace(/SECTOR:\s/, '').toLowerCase();
                        } else if (bsenseArr[3].match('SECTOR:')) {
                            balanceSheetScrapedData['industry'] = bsenseArr[3].replace(/SECTOR:\s/, '').toLowerCase();
                        }
                        else if (bsenseArr[1].match('SECTOR:')) {
                            bsenseArr[1].replace(/SECTOR:\s/, '').toLowerCase()
                        }
                        else {
                            var page_missing = { "status": "Sorry! no data available at the source", "msg": "source missing" };
                            resolve(page_missing);
                        }
                        balanceSheetScrapedData['industry'] = balanceSheetScrapedData['industry'].replace(/\t/g, '');
                        var matricArray1 = [
                            'Long Term Borrowings',
                            'Short Term Borrowings',
                            'Other Current Liabilities',
                            'Trade Receivables',
                            'Cash And Cash Equivalents',
                            'Total Assets'
                        ];
                        var matric_correct1 = [
                            'long_term_borrowings',
                            'short_term_borrowings',
                            'other_current_liabilities',
                            'trade_receivables',
                            'cash_and_cash_equivalents',
                            'total_assets'
                        ];

                        if ($('div.boxBg1 table.table4 center font').text() === 'Data Not Available for Balance Sheet') {
                            var page_missing = { "status": "Sorry! no data available for Balance sheet of "+company_name, "msg": "source missing" };
                            resolve(page_missing);
                        }
                        else {
                            for (var item in matricArray1) {
                                if ($('div.boxBg div.boxBg1 table:nth-child(3) tr td:contains(' + matricArray1[item] + ') + td').length > 0) {
                                    $('div.boxBg div.boxBg1 table:nth-child(3) tr td:contains(' + matricArray1[item] + ') + td').filter(function () {
                                        var data = $(this);
                                        var text = data.text();
                                        text = text.replace(/,/g, "");
                                        text = text.replace(/"/g, "");
                                        balanceSheetScrapedData[matric_correct1[item]] = text;
                                    });
                                }
                                else {
                                    balanceSheetScrapedData[matric_correct1[item]] = 0;
                                }
                            }

                            resolve(balanceSheetScrapedData);
                        }

                    }

                }
            });
        });
    }
}
