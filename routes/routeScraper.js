
const calculator = require('./calculator');
const mc_query = require('./mc_db_query');
var request = require('request');
var cheerio = require('cheerio');
var baseurl = 'http://www.moneycontrol.com/financials/';
var scrapedData = {};    //object to store matrices from scraped pages
var scraped_object = {};
var exact_match = false;


module.exports = {
    initScraper: function (name, scrape_urls, res) {
        if (scrape_urls.length > 1) {
            runScraper(scrape_urls[0], scrape_urls[1], scraped_response, res);
        }
        else {
            var urlArray = [];
            var url_matched = false;
            var raw_str = name;
            var alphabet;
            raw_str = raw_str.replace(/\s/g, "").toLowerCase();
            if (raw_str[0] >= 0 && raw_str[0] <= 9) {
                alphabet = 'http://www.moneycontrol.com/india/stockpricequote/' + 'others';
            }
            else {
                alphabet = 'http://www.moneycontrol.com/india/stockpricequote/' + raw_str[0].toUpperCase();
            }
            request(alphabet, function (error, response, html) {
                urlArray = []
                var i = 0;
                var hrefAttr;
                if (!error) {
                    var $ = cheerio.load(html);
                    $('div.PT15 .pcq_tbl tbody tr:not(:first-child) td a').filter(function () {
                        hrefAttr = $(this).attr('href');
                        urlArray[i++] = "'" + hrefAttr + "'";
                    });
                }
                var regex = "^" + raw_str;
                regex = new RegExp(regex);
                var count = 0;
                exact_match = false;
                var urlSplitted = [];
                var urlSplittedBuffer = [];
                console.log('raw string=' + regex);
                for (var items in urlArray) {  // urlArray code starts here 
                    urlArray[items] = urlArray[items].replace(/http:\/\/www\.moneycontrol\.com\/india\/stockpricequote\//, "");
                    var urlSplitted = urlArray[items].split("/");
                    if (urlSplitted.length == 1) { break; } //in case if last url is empty
                    if (urlSplitted[1].match(regex) && count <= 1) {
                        count++;
                        if (count <= 1) { urlSplittedBuffer = urlSplitted; }
                        if (urlSplitted[1] === raw_str) {
                            exact_match = true;
                            break;
                        }
                    }
                }
                if (count === 1 || exact_match === true) {
                    url_matched = true;
                    var balance_url = "";
                    balance_url = baseurl + urlSplittedBuffer[1] + '/balance-sheetVI/' + urlSplittedBuffer[2] + '#' + urlSplittedBuffer[2];
                    var profitloss_url = "";
                    profitloss_url = baseurl + urlSplittedBuffer[1] + '/profit-lossVI/' + urlSplittedBuffer[2] + '#' + urlSplittedBuffer[2];
                    //here goes scraping function
                    var industry_from_url = urlSplittedBuffer[0].replace(/'/, "");
                    runScraper(balance_url, profitloss_url, scraped_response, res);
                }
                else {
                    console.log('entered company not matched'); // here our response will be there if nothing matches
                    var nocompanyfound = { "status": "Sorry! no company matched the entered name", "msg": "Ensure the correct name is entered" }
                    scraped_response(nocompanyfound, res);
                }
            });
        }
    }
}



var runScraper = function (balance_url, profitloss_url, scraped_response, res) {
    request(balance_url, function (error, response, html) {
        if (!error) {
            scrapedData = {};
            var $ = cheerio.load(html);
            var company_name = $('#nChrtPrc h1').text();
            //bse nse retreival code upto line 100
            var bsense = $('#nChrtPrc div.FL.gry10').text();
            console.log(bsense);
            var bsenseArr = bsense.split("|");
            if (bsenseArr[0].match('BSE:')) {
                scrapedData['bse'] = bsenseArr[0].replace(/BSE:\s/, '');
                if (bsenseArr[1].match('NSE:')) {
                    scrapedData['nse'] = bsenseArr[1].replace(/NSE:\s/, '');
                }
                else {
                    scrapedData['nse'] = 'na';
                }
            }
            else if (bsenseArr[0].match('NSE:')) {
                scrapedData['bse'] = 'na';
                scrapedData['nse'] = bsenseArr[0].replace(/NSE:\s/, '');
            }
            scrapedData['company_name'] = company_name;
            console.log('stoch exchange data =' + scrapedData.bse + "|" + scrapedData.nse);
            //industry retreival code upto line 108
            if (bsenseArr[2].match('SECTOR:')) {
                scrapedData['industry'] = bsenseArr[2].replace(/SECTOR:\s/, '').toLowerCase();
            } else if (bsenseArr[3].match('SECTOR:')) {
                scrapedData['industry'] = bsenseArr[3].replace(/SECTOR:\s/, '').toLowerCase();
            }
            scrapedData['industry'] = scrapedData['industry'].replace(/\t/g, '');
            var matricArray1 = [
                'Long Term Borrowings',
                'Short Term Borrowings',
                'Other Current Liabilities',
                'Trade Receivables',
                'Cash And Cash Equivalents',
                'Total Assets'
            ];
            var matric_correct1 = [
                'long_term_borrowings',
                'short_term_borrowings',
                'other_current_liabilities',
                'trade_receivables',
                'cash_and_cash_equivalents',
                'total_assets'
            ];

            if ($('div.boxBg1 table.table4 center font').text() === 'Data Not Available for Balance Sheet') {
                var page_missing = { "status": "Sorry! no data available at the source", "msg": "source missing" };
                scraped_response(page_missing, res);
            }
            else {
                for (var item in matricArray1) {
                    if ($('div.boxBg div.boxBg1 table:nth-child(3) tr td:contains(' + matricArray1[item] + ') + td').length > 0) {
                        $('div.boxBg div.boxBg1 table:nth-child(3) tr td:contains(' + matricArray1[item] + ') + td').filter(function () {
                            var data = $(this);
                            var text = data.text();
                            text = text.replace(/,/g, "");
                            text = text.replace(/"/g, "");
                            scrapedData[matric_correct1[item]] = text;
                        });
                    }
                    else {
                        scrapedData[matric_correct1[item]] = 0;
                    }
                }
                //BALANCE SHEET CODE ENDS HERE
                //code for PROFIT and LOSS sheet starts here
                request(profitloss_url, function (error, response, html) {
                    console.log('reached in profit loss');
                    if (!error) {
                        var $ = cheerio.load(html);
                        var matricArray2 = [
                            'Other Income',
                            'Total Revenue'
                        ];
                        var matric_correct2 = [
                            'other_income',
                            'total_revenue'
                        ];
                        for (var item in matricArray2) {
                            if ($('div.boxBg div.boxBg1 table:nth-child(3)  tr td:contains(' + matricArray2[item] + ') + td').length > 0) {
                                $('div.boxBg div.boxBg1 table:nth-child(3)  tr td:contains(' + matricArray2[item] + ') + td').filter(function () {
                                    var data = $(this);
                                    var text = data.text();
                                    text = text.replace(/,/g, "");
                                    scrapedData[matric_correct2[item]] = text;
                                });
                            }
                            else {
                                scrapedData[matric_correct2[item]] = 0;
                            }
                        }
                        console.log(scrapedData);
                        //google finance code
                        var googleFinanceUrl = 'https://finance.google.com/finance?q=' + 'BOM:' + scrapedData.bse;
                        request(googleFinanceUrl, function (error, response, html) {
                            if (!error) {
                                var gooFinArray = [];
                                var gooFinObject = {};
                                var $ = cheerio.load(html);
                                $('.g-c .sfe-section table.quotes.rgt.nwp tbody').filter(function () {
                                    var gooFinTableBody = $(this);
                                     console.log('nnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnooooooooooooooooooooo')
                                    for (var row = 0; row < gooFinTableBody.children().length; row++) {
                                        var child = row + 1;
                                        var fieldName = gooFinTableBody.children('tr:nth-child(' + child + ')').children('td:nth-child(1)').text();
                                        fieldName = fieldName.replace(/\n/, '');
                                        var fieldValue = gooFinTableBody.children('tr:nth-child(' + child + ')').children('td:nth-child(3)').text();
                                        fieldValue = fieldValue.replace(/\n/, '');
                                        gooFinArray[row] = fieldName + ' : ' + fieldValue;
                                        console.log('fieldname=' + fieldName);
                                    }
                                    gooFinObject['gooFinArray'] = gooFinArray;
                                    //google finance top portion left side data
                                    $('div#price-panel').filter(function () {
                                        var gooDataTopLeftOne = $(this).children('div:nth-child(1)').children('span').text();
                                        var gooDataTopLeftTwo = $(this).children('div:nth-child(1)').children('.id-price-change.nwp').text();
                                        gooFinObject['gooDataTopLeftOne'] = gooDataTopLeftOne.trim().replace(/\n/, '');
                                        gooFinObject['gooDataTopLeftTwo'] = gooDataTopLeftTwo.trim().replace(/\n/, '');
                                    });

                                    //google finance top portion right side table data
                                    $('.snap-panel-and-plusone .snap-panel').filter(function () {
                                        var tableOne = [];
                                        var tableTwo = [];
                                        //table one
                                        var gooTopTableOne = $(this).children('table:nth-child(1)').children('tbody');
                                        for (var row = 0; row < gooTopTableOne.children().length; row++) {
                                            var child = row + 1;
                                            var key = gooTopTableOne.children('tr:nth-child(' + child + ')').children('.key').text().replace(/\n/, '');
                                            var value = gooTopTableOne.children('tr:nth-child(' + child + ')').children('.val').text().replace(/\n/, '');
                                            tableOne[row] = key + ' : ' + value;
                                        }
                                        gooFinObject['tableOne'] = tableOne;
                                        //table two
                                        var gooTopTableTwo = $(this).children('table:nth-child(2)').children('tbody');
                                        for (var row = 0; row < gooTopTableTwo.children().length; row++) {
                                            var child = row + 1;
                                            var key = gooTopTableTwo.children('tr:nth-child(' + child + ')').children('.key').text().replace(/\n/, '');
                                            var value = gooTopTableTwo.children('tr:nth-child(' + child + ')').children('.val').text().replace(/\n/, '');
                                            tableTwo[row] = key + ' : ' + value;
                                        }
                                        gooFinObject['tableTwo'] = tableTwo;
                                        console.log(gooFinObject);
                                    });
                                    scrapedData['googlefinance'] = gooFinObject;
                                    // calculator.calculate(scrapedData, calculated_response, res);
                                    
                                    calculator.calculate(scrapedData, calculated_response, res);
                                });
                            }
                        });
                    }
                });
            }
            console.log(scrapedData);
        }
    });
    var calculated_response = function (fineData, res) {  //used in calculator.js
        scraped_response(fineData, res);                 //defined in routes.js
    }
}




var scraped_response = function (fineData, res) {

    if (fineData.status) {
        //here the not found status object is sent to thefrontend
        res.json(fineData);
    }
    else {
        var comPar = [];
        var nComPar = [];
        var industryResult = false;
        var respObject = {};
        var status = 1;
        industryArr = [
            "alcohol",
            "bank",
            "brewery",
            "breweries",
            "distilleries",
            "banking",
            "cigarette",
            "tobacco",
            "porno",
            "pork",
            "finance",
            "financial",
            "entertainment",
            "casino",
            "weapons"
        ];
        for (var item in industryArr) {
            if (fineData.industry.match(industryArr[item])) {
                industryResult = true;
                break;
                console.log('reached');
            }
        }
        if (industryResult === true) {
            status = 0;
            nComPar[nComPar.length] = "Industry not compliant";

        }
        else {
            comPar[comPar.length] = "Industry of " + fineData.company_name + " is compliant";
        }
        if (fineData.accounts_receivables > 90) {
            status = 0;
            nComPar[nComPar.length] = "Account receiveables are beyond the limit";
        }
        else {
            comPar[comPar.length] = "Accounts receivables= " + fineData.accounts_receivables + "%, (under 90 %)";
        }
        if (fineData.interest_based_income > 5) {
            status = 0;
            nComPar[nComPar.length] = "Interest based income more than defined limit";
        }
        else {
            comPar[comPar.length] = "Interest based income within limit";
        }
        if (fineData.debt > 33) {

            status = 0;
            nComPar[nComPar.length] = "Debt more than defined limit";
        }
        else if (fineData.debt === null) {
            fineData.debt = 0;// in case if it is null to prevent conflict with type
            status = 0;
            nComPar[nComPar.length] = "No info about Debt ratio";
        }
        else {
            comPar[comPar.length] = "Debt= " + fineData.debt + "%,  (under 33%)";
        }

        if (status === 0) {
            fineData.iscompliant = "no";
        }
        else {
            fineData.iscompliant = "yes";
        }
        fineData.comPar = comPar;
        fineData.nComPar = nComPar;
        respObject["company_name"] = fineData.company_name;
        respObject["industry"] = fineData.industry;
        respObject["iscompliant"] = fineData.iscompliant;
        respObject["comPar"] = fineData.comPar;
        respObject["nComPar"] = fineData.nComPar;
        console.log('exact match = ' + exact_match);
        respObject["accounts_receivables"] = fineData.accounts_receivables;
        respObject["debt"] = fineData.debt;
        respObject["interest_based_income"] = fineData.interest_based_income;
        respObject["bse"] = fineData.bse;
        respObject["nse"] = fineData.nse;
        mc_query.addCompany(respObject);
        respObject["googlefinance"] = fineData.googlefinance;
        if (exact_match === false) {
            respObject["company_name"] = 'did you mean: ' + fineData.company_name;
        }
        res.json(respObject);
    }
}
