//This module is used when the campany typed in the input box does not match any suggestions, or the user types 
//partial string. In that case the partial name is passed to this module from routes.js file along with the 'res'.
//the logic is that the scraper picks the first alphabet or number of the name and go on searching the money control
//pages for the company name with best match alphabetically and if it finds the nearest possible name with unique
//alternatives it sends the balance sheet url and profilt sheet url for actual scraping to the mc_balancesheet_scraper.js,
//and profit_sheet_scraper.js.



var request = require('request');
var cheerio = require('cheerio');
const baseurl = 'http://www.moneycontrol.com/financials/';

module.exports = {
    initScraper: function (name) {
        return new Promise((resolve, reject) => {
            var urlArray = [];
            var url_matched = false;
            var raw_str = name;
            var alphabet;
            raw_str = raw_str.replace(/\s/g, "").toLowerCase();
            if (raw_str[0] >= 0 && raw_str[0] <= 9) {
                alphabet = 'http://www.moneycontrol.com/india/stockpricequote/' + 'others';
            }
            else {
                alphabet = 'http://www.moneycontrol.com/india/stockpricequote/' + raw_str[0].toUpperCase();
            }
            request(alphabet, function (error, response, html) {
                urlArray = [];
                var i = 0;
                var hrefAttr;
                if (!error) {
                    var $ = cheerio.load(html);
                    $('div.PT15 .pcq_tbl tbody tr:not(:first-child) td a').filter(function () {
                        hrefAttr = $(this).attr('href');
                        urlArray[i++] = "'" + hrefAttr + "'";
                    });
                }
                var regex = "^" + raw_str;
                regex = new RegExp(regex);
                var count = 0;
                exact_match = false;
                var urlSplitted = [];
                var urlSplittedBuffer = [];
                console.log('raw string=' + regex);
                for (var items in urlArray) {  // urlArray code starts here 
                    urlArray[items] = urlArray[items].replace(/http:\/\/www\.moneycontrol\.com\/india\/stockpricequote\//, "");
                    var urlSplitted = urlArray[items].split("/");
                    if (urlSplitted.length == 1) { break; } //in case if last url is empty
                    if (urlSplitted[1].match(regex) && count <= 1) {
                        count++;
                        if (count <= 1) { urlSplittedBuffer = urlSplitted; }
                        if (urlSplitted[1] === raw_str) {
                            exact_match = true;
                            break;
                        }
                    }
                }
                if (count === 1 || exact_match === true) {
                    url_matched = true;
                    var balance_url = "";
                    balance_url = baseurl + urlSplittedBuffer[1] + '/balance-sheetVI/' + urlSplittedBuffer[2] + '#' + urlSplittedBuffer[2];
                    var profitloss_url = "";
                    profitloss_url = baseurl + urlSplittedBuffer[1] + '/profit-lossVI/' + urlSplittedBuffer[2] + '#' + urlSplittedBuffer[2];
                    //here goes scraping function
                    var industry_from_url = urlSplittedBuffer[0].replace(/'/, "");
                    resolve([balance_url.replace(/'/g, ''), profitloss_url.replace(/'/g, '')]);
                }
                else {
                    console.log('entered company not matched'); // here our response will be there if nothing matches
                    var nocompanyfound = { "status": "Sorry! no company matched the entered name", "msg": "Ensure the correct name is entered" }
                    reject(nocompanyfound);
                }
            });
        });
    }
}
