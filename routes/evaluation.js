const mc_query = require('./mc_db_query');

module.exports = {
    evaluate :function (calculatedData) {

                var comPar = [];
                var nComPar = [];
                var industryResult = false;
                var respObject = {};
                var status = 1;
                var industryArr = [
                    "alcohol",
                    "bank",
                    "brewery",
                    "breweries",
                    "distilleries",
                    "banking",
                    "cigarette",
                    "tobacco",
                    "porno",
                    "pork",
                    "finance",
                    "financial",
                    "entertainment",
                    "casino",
                    "weapons"
                ];
                for (var item in industryArr) {
                    if (calculatedData.industry.match(industryArr[item])) {
                        industryResult = true;
                        break;
                        console.log('reached');
                    }
                }
                if (industryResult === true) {
                    status = 0;
                    nComPar[nComPar.length] = "Industry not compliant";
        
                }
                else {
                    comPar[comPar.length] = "Industry of " + calculatedData.company_name + " is compliant";
                }
                if (calculatedData.accounts_receivables > 90) {
                    status = 0;
                    nComPar[nComPar.length] = "Account receiveables are beyond the limit";
                }
                else {
                    comPar[comPar.length] = "Accounts receivables= " + calculatedData.accounts_receivables + "%, (under 90 %)";
                }
                if (calculatedData.interest_based_income > 5) {
                    status = 0;
                    nComPar[nComPar.length] = "Interest based income more than defined limit";
                }
                else {
                    comPar[comPar.length] = "Interest based income within limit";
                }
                if (calculatedData.debt > 33) {
        
                    status = 0;
                    nComPar[nComPar.length] = "Debt more than defined limit";
                }
                else if (calculatedData.debt === null) {
                    calculatedData.debt = 0;// in case if it is null to prevent conflict with type
                    status = 0;
                    nComPar[nComPar.length] = "No info about Debt ratio";
                }
                else {
                    comPar[comPar.length] = "Debt= " + calculatedData.debt + "%,  (under 33%)";
                }
        
                if (status === 0) {
                    calculatedData.iscompliant = "no";
                }
                else {
                    calculatedData.iscompliant = "yes";
                }
                calculatedData.comPar = comPar;
                calculatedData.nComPar = nComPar;
                respObject["company_name"] = calculatedData.company_name;
                respObject["industry"] = calculatedData.industry;
                respObject["iscompliant"] = calculatedData.iscompliant;
                respObject["comPar"] = calculatedData.comPar;
                respObject["nComPar"] = calculatedData.nComPar;
                respObject["accounts_receivables"] = calculatedData.accounts_receivables;
                respObject["debt"] = calculatedData.debt;
                respObject["interest_based_income"] = calculatedData.interest_based_income;
                respObject["bse"] = calculatedData.bse;
                respObject["nse"] = calculatedData.nse;
                mc_query.addCompany(respObject);
                return(respObject);
        }
}
