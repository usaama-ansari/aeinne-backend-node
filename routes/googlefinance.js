const calculator = require('./calculator');
const mc_query = require('./mc_db_query');
var request = require('request');
var cheerio = require('cheerio');



module.exports = googleFinance = {
    getGoogleFinance: function (googleFinanceUrl) {
        return new Promise((resolve, reject) => {
            request(googleFinanceUrl, function (error, response, html) {
                if (!error) {
                    var gooFinObject = {};
                    var gooFinArray = [];
                    var $ = cheerio.load(html);
                    $('.g-c .sfe-section table.quotes.rgt.nwp tbody').filter(function () {
                        var gooFinTableBody = $(this);
                        for (var row = 0; row < gooFinTableBody.children().length; row++) {
                            var child = row + 1;
                            var fieldName = gooFinTableBody.children('tr:nth-child(' + child + ')').children('td:nth-child(1)').text();
                            fieldName = fieldName.replace(/\n/, '');
                            var fieldValue = gooFinTableBody.children('tr:nth-child(' + child + ')').children('td:nth-child(3)').text();
                            fieldValue = fieldValue.replace(/\n/, '');
                            gooFinArray[row] = fieldName + ' : ' + fieldValue;
                            console.log('fieldname=' + fieldName);
                        }
                        gooFinObject['gooFinArray'] = gooFinArray;
                    });
                    //google finance top portion left side data
                    $('div#price-panel').filter(function () {
                        var gooDataTopLeftOne = $(this).children('div:nth-child(1)').children('span').text();
                        var gooDataTopLeftTwo = $(this).children('div:nth-child(1)').children('.id-price-change.nwp').text();
                        gooFinObject['gooDataTopLeftOne'] = gooDataTopLeftOne.trim().replace(/\n/, '');
                        gooFinObject['gooDataTopLeftTwo'] = gooDataTopLeftTwo.trim().replace(/\n/, '');
                    });

                    //google finance top portion right side table data
                    $('.snap-panel-and-plusone .snap-panel').filter(function () {
                        var tableOne = [];
                        var tableTwo = [];
                        //table one
                        var gooTopTableOne = $(this).children('table:nth-child(1)').children('tbody');
                        for (var row = 0; row < gooTopTableOne.children().length; row++) {
                            var child = row + 1;
                            var key = gooTopTableOne.children('tr:nth-child(' + child + ')').children('.key').text().replace(/\n/, '');
                            var value = gooTopTableOne.children('tr:nth-child(' + child + ')').children('.val').text().replace(/\n/, '');
                            tableOne[row] = key + ' : ' + value;
                        }
                        gooFinObject['tableOne'] = tableOne;
                        //table two
                        var gooTopTableTwo = $(this).children('table:nth-child(2)').children('tbody');
                        for (var row = 0; row < gooTopTableTwo.children().length; row++) {
                            var child = row + 1;
                            var key = gooTopTableTwo.children('tr:nth-child(' + child + ')').children('.key').text().replace(/\n/, '');
                            var value = gooTopTableTwo.children('tr:nth-child(' + child + ')').children('.val').text().replace(/\n/, '');
                            tableTwo[row] = key + ' : ' + value;
                        }
                        gooFinObject['tableTwo'] = tableTwo;
                    });
                    resolve(gooFinObject);
                }
            });
        });
    }
}


