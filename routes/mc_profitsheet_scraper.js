var request = require('request');
var cheerio = require('cheerio');




module.exports = {

    profitSheetScrape: function (profitloss_sheet_url) {       
        return new Promise((resolve, reject) => {
            request(profitloss_sheet_url, function (error, response, html) {
                profitSheetScrapedData={};
                console.log('reached in profit loss');
                if (!error) {
                    var $ = cheerio.load(html);
                    var matricArray2 = [
                        'Other Income',
                        'Total Revenue'
                    ];
                    var matric_correct2 = [
                        'other_income',
                        'total_revenue'
                    ];
                    for (var item in matricArray2) {
                        if ($('div.boxBg div.boxBg1 table:nth-child(3)  tr td:contains(' + matricArray2[item] + ') + td').length > 0) {
                            $('div.boxBg div.boxBg1 table:nth-child(3)  tr td:contains(' + matricArray2[item] + ') + td').filter(function () {
                                var data = $(this);
                                var text = data.text();
                                text = text.replace(/,/g, "");
                                profitSheetScrapedData[matric_correct2[item]] = text;
                            });
                        }
                        else {
                            profitSheetScrapedData[matric_correct2[item]] = 0;
                        }
                    }
                    resolve(profitSheetScrapedData);
                }
            });
        });
    }
}