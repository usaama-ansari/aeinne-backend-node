const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var suggestionSchema = new Schema({
company_name : {
    type: String,
    required: true,
    unique: true
},
url_name: {
    type: String,
    required: true,
},
bse: {
    type: String,
    required: true,
},
nse:{
    type: String,
    required: true
},
comp_code: {
    type: String,
    required: true
}
});
var suggestion = mongoose.model('Suggestion',suggestionSchema);
module.exports = suggestion;