const mongoose = require('mongoose');
var Schema = mongoose.Schema;


var companySchema = new Schema({
    company_name: {
        type: String,
        unique: true,
        required: true
    },
    bse:{
        type: String,
        required:true
    },
    nse:{
        type: String,
        required:true
    },
    industry: {
        type: String,
        required: true
    },
    accounts_receivables: {
        type: Number,
        required: true
    },
    interest_based_income: {
        type: Number,
        required: true
    },
    debt: {
        type: Number,
        required: true
    },
    iscompliant: {
        type: String,
        required: true
    },
    nComPar: [],
    comPar: []
});


var Company = mongoose.model('Company', companySchema);

// make this available to our users in our Node applications
module.exports = Company;




